#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <unistd.h>

struct user
{
	char username[30];
	char password[30];
	int mobile;
	char email[30];
	int age;
};

struct admin
{
	char name[30];
	char password[30];
};

struct food_details
{
	char name[30];
	int price;
	int id;
};

void addfood();
void deletefood();
void Admin();
void User();
void Food_Order();
void Signup();
void Login();
void Signup_user();
void Login_user();
void mainmenu();
void modifyfood();
void display_all_foods();
void food_purchase_history();


int main()
{
system("clear");
printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
mainmenu();
return 0;
}

void mainmenu()
{
while(1)
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	int choice;
	printf(" 1. Admin\n 2. User\n 3. Exit\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: Admin();
			break;

		case 2: User();
			break;

		case 3: exit(0);
			break;

		default: printf("Enter valid choice\n");
	        	break;
	}
	
}

}


void Admin()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	int choice;
	printf("Welcome to Admin Section\n");
	printf(" 1. Admin Signup\n 2. Admin Login\n 3. Go to Main-Menu\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: Signup();
			break;

		case 2: Login();
			break;

		case 3: mainmenu();
			break;

		default: printf("Enter Valid Choice\n");
			 break;
	}
}
	

void User()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	int choice;
	printf("Welcome to User Section\n");
	printf(" 1. User Signup\n 2. User Login\n 3. Go to Main-Menu\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: Signup_user();
			break;

		case 2: Login_user();
			break;

		case 3: mainmenu();
			break;

		default: printf("Enter Valid Choice\n");
			 break;
	}
}

void Signup_user()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	struct user u,temp_u;
	FILE *fp;
	fp=fopen("User_details.txt","r+");
	printf("\n\n");
 	printf("\t\t\t\t\t\t\t\t\t\t\t*********Signin Details*********\t\t\t\n");
	printf("Enter Username:");
	scanf("%s",u.username);
	printf("Enter Age:");
	scanf("%d",&u.age);
	printf("Enter Email:");
	scanf("%s",u.email);
	printf("Enter Mobile Number:");
	scanf("%d",&u.mobile);
	printf("Enter Password:");
	scanf("%s",u.password);
	while(fread(&temp_u,sizeof(struct user),1,fp)!='\0')
	{
		if(strcmp(temp_u.username,u.username)==0)
		{
			printf("\nUsername Already Exist, Please Signin with another Username\n");
			sleep(5);
			return;
		}
	}
	fwrite(&u,sizeof(struct user),1,fp);
	printf("\nSuccessfully Signed Up\n");
	fclose(fp);
	sleep(5);
	
	
}

void Login_user()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp;
	struct user i;
	char username[30],password[30];
	fp=fopen("User_details.txt","r");
	printf("\n\n");
  	printf("\t\t\t\t\t\t\t\t\t\t\t*********Login Details*********\t\t\t\n");
	printf("Enter Username:");
	scanf("%s",username);
	printf("Enter Password:");
	scanf("%s",password);
	while(fread(&i,sizeof(struct user),1,fp)!='\0')
	{
		if((strcmp(i.username,username)==0 && strcmp(i.password,password)==0))
		{
			printf("\nLog in Successful\n");
			sleep(5);
			Food_Order();
			return;
		}
		
	}
	fclose(fp);
	printf("\n");
	printf("Login Failed\n");
	sleep(5);
	
}


void Food_Order()
{
	system("clear");
	int choice;
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\n\n");
	FILE *fp,*history_fp;
	struct food_details f;
	int choice_id,quantity,to_continue,total_bill=0;
	fp=fopen("Food_Record.txt","r");
	history_fp=fopen("purchase_history.txt","a+");
	printf("ID   Food Name Price\n");
	while((fread(&f,sizeof(struct food_details),1,fp)))
	{
        	printf("%d\t%s\t%d\n",f.id,f.name,f.price);
	}
	printf("\n\n");
	fseek(fp,0,SEEK_SET);
	while(1)
	{
		printf("Enter Food ID which you want to order?\n");
		scanf("%d",&choice_id);
		printf("ID  Food Name Price\n");
		while(fread(&f,sizeof(struct food_details),1,fp))
		{
			if(f.id==choice_id)
			{
				rewind(fp);
				printf("\n");
				//fseek(fp,0,SEEK_SET);
				printf("%d\t%s\t%d\n",f.id,f.name,f.price);
				printf("\n");
		                printf("Enter the no of Quantity you want to purchase?\n");
				scanf("%d",&quantity);
				printf("\n");
				total_bill+=f.price*quantity;
				printf("\n");
				//Writing Order in purchase_history file
				fprintf(history_fp,"%d\t%s\t%d\t%d\t%d\n",f.id,f.name,quantity,f.price,(f.price*quantity));
		 		printf("Do you want to Order more food Items?(YES=1,NO=0)\n");
				scanf("%d",&to_continue);
				break;
			}

		}

		if(to_continue==0)
		{
			break;
		}
			
	}
	fclose(fp);
	fclose(history_fp);
	printf("\n\n");
	printf("Total Cart Value is:%d\n",total_bill);
	/*printf("\t\t\t\t\t\t\t\t\t\t\t***************************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\tYour Order will Reach within 30 minutes\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t***************************************\t\t\t\n");
	*/
	printf("\n\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*************************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Thank You for Ordering from Saffron \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*************************************\t\t\t\n");
	printf("\n");
	printf("1. Go to Main-Menu\n2. Exit\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: mainmenu();
			break;

		case 2: exit(0);
			break;

		default: printf("Please Enter Valid Choice\n");
			 break;
	}
}



	
void Signup()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp,*poweruser_fp;
	char poweruser_password[30],file_poweruser_password[30];
	struct admin a_signup,temp_a;
	printf("Enter Password for Power-User:");
	scanf("%s",poweruser_password);
	poweruser_fp=fopen("power_user.txt","r");
	fscanf(poweruser_fp,"%s",file_poweruser_password);
	printf("\n");
	if(strcmp(file_poweruser_password,poweruser_password)==0)
	{

 		printf("\t\t\t\t\t\t\t\t\t\t\t*********Signin Details*********\t\t\t\n");
		printf("\n");
		printf("\nNow, You can access Admin-SignUp\n");
		printf("Enter Name of Admin:");
		scanf("%s",a_signup.name);
		printf("Enter Password:");
		scanf("%s",a_signup.password);
		fp=fopen("Admin_Record.txt","r+");
		while(fread(&temp_a,sizeof(struct admin),1,fp)!='\0')
		{
			if(strcmp(a_signup.name,temp_a.name)==0)
			{	
				printf("\nAdmin Username Aready Exist,Please Login with another Username\n");
				sleep(5);
				return;
			}
		}
		fwrite(&a_signup,sizeof(struct admin),1,fp);
		printf("\nSuccessfully Signed Up\n");
		sleep(5);
		fclose(fp);
	}
	else
	{
		printf("You cannot Signup as Password is Invalid\n");
		sleep(5);
	}
}

void Login()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp;
	struct admin a_login;
	char username[30],password[30];
	fp=fopen("Admin_Record.txt","r");
	printf("\n\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*********Login Details*********\t\t\t\n");
	printf("Enter Username:");
	scanf("%s",username);
	printf("Enter Password:");
	scanf("%s",password);
	rewind(fp);
	while(fread(&a_login,sizeof(struct admin),1,fp)!='\0')
	{	
		if((strcmp(a_login.name,username)==0 && strcmp(a_login.password,password)==0))
		{
			printf("\n");
			printf("Log in Successful\n");
			sleep(5);
			int choice,again,menu;
			printf("\nHello,%s\n",a_login.name);
			printf(" 1. Add Food\n 2. Delete Food\n 3. Modify Food\n 4. Food Purchase History\n 5. Go to Main-menu\n 6. Exit\n");
			scanf("%d",&choice);
		label:	switch(choice)
			{
				case 1: addfood();
					break;

				case 2: deletefood();
					break;

				case 3: modifyfood();
					break;

				case 4: food_purchase_history();
					break;
			}
			printf("\nDo you want to Continue further?(YES=1,NO=0)\n");
			scanf("%d",&again);
			if(again==1)
			{
				printf(" 1. Add Food\n 2. Delete Food\n 3. Modify Food\n 4. Food Purchase History\n");
				scanf("%d",&choice);
				goto label;				
			}
			printf("\n\n");
			printf("1.Go to Main-Menu\n2.Exit\n");
			scanf("%d",&menu);
			switch(menu)
			{

				case 1: mainmenu();
					break;

				case 2: exit(0);
					break;

				default:printf("Enter Valid Choice\n");
			 		break;
			}		
			fclose(fp);
			return;
		}
	
	}
	printf("\nLogin Failed\n");
	sleep(5);
	
}



void addfood()
{
	system("clear");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp;
	int choice;
	struct food_details f;
	printf("\n");
	do
	{

 		printf("\t\t\t\t\t\t\t\t\t\t\t***********Add Food***********\t\t\t\n");
		printf("Enter Food ID:");
		scanf("%d",&f.id);
		printf("Enter Food Name:");
		scanf("%s",f.name);
		printf("Enter Food Price:");
		scanf("%d",&f.price);
		fp=fopen("Food_Record.txt","a+");
		fwrite(&f,sizeof(struct food_details),1,fp);
		printf("\n");
		printf("Do you want to add more Foods?(YES=1,NO=0)\n");
		scanf("%d",&choice);
		fclose(fp);
	}while(choice!=0);

		/*printf("\n\n");
		printf("1. Go to Main-Menu\n2. Exit\n");
		scanf("%d",&choice);
		switch(choice)
		{
			case 1: mainmenu();
				break;

			case 2: exit(0);
				break;

			default: printf("Please Enter Valid Choice\n");
				 break;
		}
		*/

}
	

void deletefood()
{
	system("clear");
	int choice;
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp,*temp_fp;
	int id;
	struct food_details f;
	printf("\n");
 	printf("\t\t\t\t\t\t\t\t\t\t\t***********Delete Food***********\t\t\t\n");
	display_all_foods();
	printf("Enter ID Number:");
	scanf("%d",&id);
	printf("\n");
	fp=fopen("Food_Record.txt","r+");
	temp_fp=fopen("temp_Food_Record.txt","a+");
	printf("\n\nAfter Deleting Particular Food\n\n");
	printf("ID Food Name Price\n");
	while(fread(&f,sizeof(struct food_details),1,fp)!='\0')
	{
		if(f.id != id)
		{
			printf("%d\t%s\t%d\n",f.id,f.name,f.price);
			fwrite(&f,sizeof(struct food_details),1,temp_fp);
		}
	}
	fclose(fp);
	fclose(temp_fp);
	remove("Food_Record.txt");
	rename("temp_Food_Record.txt","Food_Record.txt");
	/*printf("\n\n");
	printf("1. Go to Main-Menu\n 2. Exit\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: mainmenu();
			break;

		case 2: exit(0);
			break;

		default: printf("Please Enter Valid Choice\n");
			 break;
	}
	*/
}


void display_all_foods()
{
	FILE *fp;
	struct food_details f;
	fp=fopen("Food_Record.txt","r");
	printf("ID Food Name Price\n");
	while((fread(&f,sizeof(struct food_details),1,fp)))
	{
        	printf("%d\t%s\t%d\n",f.id,f.name,f.price);
	}
	fclose(fp);
}
	


void modifyfood()
{
	system("clear");
	int choice;
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t Welcome to Saffron Food Order \t\t\t\n");
	printf("\t\t\t\t\t\t\t\t\t\t\t*******************************\t\t\t\n");
	FILE *fp;
	int id,new_price;
	struct food_details f;
	printf("\n");
 	printf("\t\t\t\t\t\t\t\t\t\t\t***********Modify Food***********\t\t\t\n");
	display_all_foods();
	printf("\nEnter ID Number:");
	scanf("%d",&id);
	printf("\nEnter New Price of that food:");
	scanf("%d",&new_price);
	printf("\n");
	fp=fopen("Food_Record.txt","r+");
	printf("\n\nAfter Modifying Food Price\n\n");
	while(fread(&f,sizeof(struct food_details),1,fp)!='\0')
	{
		if(f.id == id)
		{
			fseek(fp,-(sizeof(struct food_details)),SEEK_CUR);
			f.price=new_price;
			fwrite(&f,sizeof(struct food_details),1,fp);
		}
	}
	display_all_foods();
	fclose(fp);
	/*printf("\n\n");
	printf("1. Go to Main-Menu\n2. Exit\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: mainmenu();
			break;

		case 2: exit(0);
			break;

		default: printf("Please Enter Valid Choice\n");
			 break;
	}
	*/
}

void food_purchase_history()
{
	FILE *history_fp;
	int id,quantity,price,total_value,choice;
	char name[30];
	history_fp=fopen("purchase_history.txt","r+");
	printf("\nID Food Name Quantity Price Total Price\n");
	while(1)
	{
		if(feof(history_fp))
			break;
		fscanf(history_fp,"%d\t%s\t%d\t%d\t%d\n",&id,name,&quantity,&price,&total_value);
		printf("%d\t%s\t%d\t%d\t%d\n",id,name,quantity,price,total_value);
	}
	fclose(history_fp);	
	/*printf("\n\n");
	printf("1. Go to Main-Menu\n2. Exit\n");
	scanf("%d",&choice);
	switch(choice)
	{
		case 1: mainmenu();
			break;

		case 2: exit(0);
			break;

		default: printf("Please Enter Valid Choice\n");
			 break;
	}
	*/
}

