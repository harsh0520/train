#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include<assert.h>
#define MAX_CHARACTERS 1005
#define MAX_PARAGRAPHS 5

char* kth_word_in_mth_sentence_of_nth_paragraph(char**** document, int k, int m, int n) 
{
    return document[n-1][m-1][k-1];
}

char** kth_sentence_in_mth_paragraph(char**** document, int k, int m) 
{ 
    return document[m-1][k-1];  
}

char*** kth_paragraph(char**** document, int k) 
{
    return document[k-1];
}

char* word_function(char *text,int start,int end)  // Word Function
{
     char *result;
    int i,y=0,z;
    z=(end-start+1);
    
    result = malloc(z*sizeof(char));  // example-> word = "LEARNING", Letters=8, start=0, end=7 (range=0-7)  
                                      // so, end-start=7, +1 is added to include '\0' in the string. (end-start+1)
    
    for(i=start;i<=end;i++)
    {   
        result[y]= text[i];
        y++;
    }
    
    result[y]='\0'; // To put '\0' at the end to tell end to document.
    return result;
}


char** sentence_function(char *text,int start,int end)   // Sentence Function
{
    char **result;
    int i,x=start,y=0,words=1;

    for(i=start;i<=end;i++)
    {
        if(text[i] == ' ')  // whitespace indicates word completed.
        {
            words++;
        }
    }

    result = malloc(words*sizeof(char*));

    for(i=start;i<=end;i++)
    {
        if(text[i] == ' ')
        {
            result[y]= word_function(text,x,i-1); // word's are here stored in form of array.                                                                             // result[0],result[1] ..... etc.
            y++;
            x=i+1;
        }
    }
    result[y]=word_function(text,x,i-1); // This return is required for example when we write last word, We don't give whitespace,So it will not consider it as a word. So, to include that, We use this return. This is similar to what we have done for paragraph.
    return result;

}

char*** paragraph_function(char *text,int start,int end)  // Paragraph Function
{
    char ***result;
    int i,y=0,x=start,sentence=0;

    for(i=start;i<=end;i++)
    {
        if(text[i] == '.')  // '.' indicates sentence completed.
        {
            sentence++;
        }
    }

    result = malloc(sentence*sizeof(char**));

    for(i=start;i<=end;i++)
    {
        if(text[i] == '.')
        {
            result[y]= sentence_function(text,x,i-1); // sentence's are here stored in form of array.                                                                         // result[0],result[1] ..... etc.
            y++;
            x=i+1;  // earlier i-1 indicates end. now x=i+1. It is used to take next character(beg) till
        }
    }

    return result;

}

char**** get_document(char* text) 
{
    char ****result;
    int paragraph=1,i=0,x=0,y=0;

    while(text[i])
    {
        if(text[i] == '\n')   // '\n' indicates paragraph completed.
        {
            paragraph++; 
        }
        i++;
    }
    
    result = malloc(paragraph*sizeof(char***));
    
    i=0;                   
    while(text[i])
    {
        if(text[i] == '\n')
        {
            result[y]= paragraph_function(text,x,i-1); // paragraph's are here stored in form of array.                                                                        // result[0],result[1] ..... etc.
            y++;  
            x=i+1;
        }
        i++;
    }
    
    result[y]= paragraph_function(text,x,i-1); // This return is required for example when we write 2nd paragraph of         single line and we don't give '\n'. It will not consider as paragraph. So, to include that, We use this return.                         
    return result;     
}


char* get_input_text() 
{	
    int paragraph_count;
    scanf("%d", &paragraph_count);
    char p[MAX_PARAGRAPHS][MAX_CHARACTERS], doc[MAX_CHARACTERS];
    memset(doc, 0, sizeof(doc));
    getchar();
    for (int i = 0; i < paragraph_count; i++) 
    {
        scanf("%[^\n]%*c", p[i]);
        strcat(doc, p[i]);
        if (i != paragraph_count - 1)
            strcat(doc, "\n");
    }

    char* returnDoc = (char*)malloc((strlen (doc)+1) * (sizeof(char)));
    strcpy(returnDoc, doc);
    return returnDoc;
}

void print_word(char* word) 
{
    printf("%s", word);
}

void print_sentence(char** sentence) 
{
    int word_count;
    scanf("%d", &word_count);
    for(int i = 0; i < word_count; i++)
    {
        printf("%s", sentence[i]);
        if( i != word_count - 1)
            printf(" ");
    }
} 

void print_paragraph(char*** paragraph) 
{
    int sentence_count;
    scanf("%d", &sentence_count);
    for (int i = 0; i < sentence_count; i++) 
    {
        print_sentence(*(paragraph + i));
        printf(".");
    }
}

// Main Code of Querying the Document
int main() 
{
    char* text = get_input_text();
    char**** document = get_document(text);

    int q;
    scanf("%d", &q);

    while (q--) 
    {
        int type;
        scanf("%d", &type);

        if (type == 3)
        {
            int k, m, n;
            scanf("%d %d %d", &k, &m, &n);
            char* word = kth_word_in_mth_sentence_of_nth_paragraph(document, k, m, n);
            print_word(word);
        }

        else if (type == 2)
        {
            int k, m;
            scanf("%d %d", &k, &m);
            char** sentence = kth_sentence_in_mth_paragraph(document, k, m);
            print_sentence(sentence);
        }

        else
        {
            int k;
            scanf("%d", &k);
            char*** paragraph = kth_paragraph(document, k);
            print_paragraph(paragraph);
        }
        printf("\n");
    }     
}
